class crea_index_php {
#	file{"/www/index.php":
#		ensure	=> file,
#		replace	=> true,
#               content => "<?php echo 'Bienvenido a Centos.dev';\n ?>"
#        }

	file{"var/www/dev":
		ensure	=> "directory",
	}

        file{"var/www/dev/index.php":
		ensure	=> file,
		replace	=> true,
		source	=> "puppet:///modules/crea_index_php/index.php",
                require => File["var/www/dev"],
        }
}

